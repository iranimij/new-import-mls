<?php

namespace RB_PLUGIN_IMPORT_MLS\Classes;

use RB_PLUGIN_IMPORT_MLS\Admin\Setting;

class Core {
	/**
	 * @var mixed|void
	 */
	public static $options;
	/**
	 * @var mixed|void
	 */
	public static $old_option;

	/**
	 * Core constructor.
	 */
	public function __construct() {
		self::$old_option = get_option( 'theme_options' );
		self::$options    = get_option( Setting::$key );
	}
}

new Core();
