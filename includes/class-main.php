<?php

class RB_Plugin_IMPORT_MLS {
	/**
	 * Plugin instance.
	 *
	 * @see get_instance()
	 * @type object
	 */
	protected static $instance = null;

	/**
	 * Access this plugin’s working instance
	 *
	 * @wp-hook plugins_loaded
	 * @since   2012.09.13
	 * @return  object of this class
	 */
	public static function get_instance() {
		null === self::$instance and self::$instance = new self;

		return self::$instance;
	}

	/**
	 * Used for regular plugin work.
	 *
	 * @wp-hook plugins_loaded
	 * @return  void
	 * @throws Exception
	 */
	public function plugin_setup() {
		$this->load_language( 'rb-wp-plugin-import-mls' );
		$this->includes();
	}

	/**
	 * Loads translation file.
	 *
	 * Accessible to other classes to load different language files (admin and
	 * front-end for example).
	 *
	 * @param $domain
	 */
	public function load_language( $domain ) {
		load_plugin_textdomain( $domain, false, RB_PLUGIN_IMPORT_MLS_PATH . '/languages' );
	}

	/**
	 * Includes plugin files
	 *
	 * @param  Not param
	 */
	public function includes() {
		// Include the vendor
		if ( file_exists( RB_PLUGIN_IMPORT_MLS_PATH . 'vendor/autoload.php' ) ) {
			include RB_PLUGIN_IMPORT_MLS_PATH . 'vendor/autoload.php';
		}

		require_once 'classes/class-setting.php';
		require_once 'classes/class-option.php';
		require_once 'classes/class-core.php';
		require_once 'classes/class-helper.php';
		require_once 'classes/class-graphql.php';
		require_once 'classes/class-listing.php';

		if ( is_admin() ) {
			require_once 'admin/class-importer.php';
		}

		// REST API
		require_once 'classes/rest-api/class-restapi.php';
		require_once 'classes/rest-api/class-restapi-listings.php';
	}
}

add_action( 'plugins_loaded', array( RB_Plugin_IMPORT_MLS::get_instance(), 'plugin_setup' ) );
