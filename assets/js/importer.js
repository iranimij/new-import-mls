jQuery(document).ready(function ($) {

	// Set Value To tml5 Form
	function set_value_input(val, type, name) {
		if (type == "text" || type == "textarea" || type == "select") {
			jQuery((type == "text" ? "input" : type) + "[name=" + name + "]").removeAttr('placeholder').val(val);
		}
		if (type == "checkbox") {
			if (val == 'Yes') {
				jQuery(type + "[name=" + name + "]").attr('checked', 'checked');
			} else {
				jQuery(type + "[name=" + name + "]").attr('checked', 'checked');
			}
		}
		if (type == "radio") {
			if (val == 'Yes') {
				jQuery("#_rb_property_view1").attr('checked', 'checked');
			} else {
				jQuery("#_rb_property_view2").attr('checked', 'checked');
			}
		}
		if (type == "tinymce") {
			var content = val.toString();
			var activeEditor = tinyMCE.get(name);

			activeEditor.setContent(content);
			jQuery('#' + name).val(content);
		}
	}

	// Click Event
	$('#rb-api-import').click(function () {

		// Get MLS number.
		var mls_number = jQuery('#_rb_mls_number').val();
		if (!mls_number) {
			alert('Please enter the MLS Number.');
			return false;
		}

		// Disable for Wait Request
		$(this).attr('disabled', 'disabled');
		$('#_rb_mls_number').attr('disabled', 'disabled');
		$('#rb-api-loading').show();

		$.ajax({
			type: 'GET',
			url: rb_rest_api.rest_url,
			data: {
				'listing_mls_number': mls_number
			},
			complete: function () {
				jQuery('#rb-api-import').removeAttr('disabled');
				jQuery('#_rb_mls_number').removeAttr('disabled');
				jQuery('#rb-api-loading').hide();
			},
			success: function (data) {
				if (typeof data['objects'][0] == 'undefined') {
          jQuery("#success_mls_import").html('Not Found').fadeIn('slow', function () {
            jQuery(this).delay(3500).fadeOut('slow');
          });
					return false
				}

				var response = data['objects'][0];
				var fields = rb_rest_api.fields;

				// Set data.
				jQuery("label#title-prompt-text").html('');
				// set_value_input(response.location.address, 'text', 'post_title');
				set_value_input(response.location.address+' '+response.location.area, fields['address']['type'], fields['address']['name']);
				set_value_input(response.location.address, fields['post_title']['type'], fields['post_title']['name']);
				set_value_input(response.info.frontage, fields['frontage']['type'], fields['frontage']['name']);
				set_value_input(response.info.publicRemarks, fields['post_content']['type'], fields['post_content']['name']);
				// set_value_input(response.info.titleToLand, fields['title']['type'], fields['title']['name']);
				set_value_input(response.location.postalCode, fields['postal_code']['type'], fields['postal_code']['name']);
				set_value_input(mls_number, fields['mls_number']['type'], fields['mls_number']['name']);
				set_value_input(response.firm_1.name, fields['brokerage_name']['type'], fields['brokerage_name']['name']);
				set_value_input(response.info.virtualTourUrl, fields['open_virtual_tour']['type'], fields['open_virtual_tour']['name']);
				set_value_input(response.listPrice, fields['price']['type'], fields['price']['name']);
				set_value_input(response.info.totalBedrooms, fields['bedrooms_number']['type'], fields['bedrooms_number']['name']);
				set_value_input(response.info.totalBaths, fields['bathrooms_number']['type'], fields['bathrooms_number']['name']);
				set_value_input(response.info.lot.size, fields['lot_size']['type'], fields['lot_size']['name']);
				set_value_input(response.info.styleOfHome[0], fields['style']['type'], fields['style']['name']);
				set_value_input(response.info.depth, fields['depth_size']['type'], fields['depth_size']['name']);
				set_value_input(response.info.floorArea.grandTotal, fields['property_size']['type'], fields['property_size']['name']);
				set_value_input(response.info.view, fields['view']['type'], fields['view']['name']);
				set_value_input(response.info.viewSpecify, fields['view_specify']['type'], fields['view_specify']['name']);
				set_value_input(response.info.approxYearBuilt, fields['built_year']['type'], fields['built_year']['name']);
				set_value_input(response.info.strataMaintFee, fields['maintenance_fee']['type'], fields['maintenance_fee']['name']);
				set_value_input(response.info.forTaxYear, fields['tax_year']['type'], fields['tax_year']['name']);
				set_value_input(response.info.grossTaxes, fields['tax']['type'], fields['tax']['name']);
				if (response.info.amenities !== null) {
					set_value_input(response.info.amenities, fields['amenities']['type'], fields['amenities']['name']);
				}
				if (response.info.featuresIncluded !== null) {
					set_value_input(response.info.featuresIncluded, fields['features']['type'], fields['features']['name']);
				}
				set_value_input(response.location.coordinate.latitude, fields['latitude']['type'], fields['latitude']['name']);
				set_value_input(response.location.coordinate.longitude, fields['longitude']['type'], fields['longitude']['name']);
				if (response.info.photo.urls.length >0) { // Upload image
					rb_upload_image(response.info.photo.urls, mls_number)
				}

				jQuery('input[name="tax_input[neighbourhood-query][]"]').each(function (index,val) {
					var thisVal = jQuery(this).parents(".selectit").text();

					if (thisVal.trim().toLowerCase() == response.location.area.toLowerCase()){
						console.log(thisVal.trim());
						if (jQuery(this).prop('checked') !== true){
							jQuery(this).delay(1000).trigger("click");
						}
					}
					if (thisVal.trim().toLowerCase() == response.location.subAreaOrCommunity.toLowerCase()){
						console.log(thisVal.trim());
						if (jQuery(this).prop('checked') !== true){
								jQuery(this).delay(1000).trigger("click");
						}
					}
				});

				jQuery('input[name="tax_input[dwelling-type][]"]').each(function (index,val) {
					var thisVal = jQuery(this).parents(".selectit").text();

					if (thisVal.trim() == response.info.typeOfDwelling){
						jQuery(this).trigger("click");
					}
				});

				jQuery('input[name="tax_input[property-type][]"]').each(function (index,val) {
					var thisVal = jQuery(this).parents(".selectit").text();

					if (thisVal.trim() == response.info.propType){
						jQuery(this).trigger("click");
					}
				});

				// Show Alert Import
				jQuery("#success_mls_import").html('Imported '+mls_number+' MLS Number.').fadeIn('slow', function () {
					jQuery(this).delay(3500).fadeOut('slow');
				});

				//
				//jQuery('#_rb_property_brokerage_name').val(response)
				//jQuery('#_rb_property_open_house_description').val(response)
				//jQuery('#_rb_property_size').val()
				//jQuery('#_rb_lot_dimension').val(response)
				//jQuery('#_rb_property_parking_number').val(response)
				//jQuery('#_rb_property_features_embed').val(response.info.virtualTourUrl)
			},
			error: function (request, status, error) {
				//var response = jQuery.parseJSON(request.responseText)
				console.log(request);
				console.log(status);
				console.log(error);
				alert('Error! The item not found!');
			}
		});

		return false;
	});

	function rb_upload_image(urls) {
		// var post_id = $('#post_ID').val();
		// 		//
		// 		// if (!post_id) {
		// 		// 	alert('Post ID not found! please try again.');
		// 		// 	return;
		// 		// }
		$('.img-status.cmb2-media-item').remove();
		$('#_rb_property_gallery-status').after('<p id="rb-importer-image-p">Please wait to download images...</p>');

		$.ajax({
			type: "get",
			dataType: "json",
			url: cmb2_l10.ajaxurl,
			timeout: 300000,
			data: {
				action: "rb_upload_image",
				post_id: 0,
				urls: urls,
			},
			success: function (response) {
				if (response.error) {
					alert(response.message);
					return
				}

				var fields = rb_rest_api.fields;
				$.each(response.data, function (key, value) {
					$('#_rb_property_gallery-status').append('<li class="img-status cmb2-media-item">\n' +
						'\t\t<img width="100" height="100" src="' + value + '" class="cmb-file_list-field-image">\n' +
						'\t\t<p><a href="#" class="cmb2-remove-file-button" rel="_rb_property_gallery[' + key + ']">Remove Image</a></p>\n' +
						'\t\t<input type="hidden" id="filelist-' + key + '" data-id="' + key + '" name="' + fields['gallery']['name'] + '[' + key + ']" value="' + value + '">\n' +
						'\t</li>')
				});

				if (response.error == false) {
					$('#rb-importer-image-p').hide();
				}
			}
		})
	}
});
