<?php

namespace RB_PLUGIN_IMPORT_MLS\Classes\API;

use RB_PLUGIN_IMPORT_MLS\Classes\GraphQL;

class Listings extends REST {
	public function __construct() {
		add_action( 'rest_api_init', array( $this, 'register_routes' ) );
	}

	public function register_routes() {
		register_rest_route( 'mls/v1', '/listing', array(
			'methods'  => 'GET',
			'callback' => array( $this, 'get_listing' ),
		) );
	}

	/**
	 * @param \WP_REST_Request $request
	 * @return array|mixed|object|\WP_Error
	 */
	public function get_listing( \WP_REST_Request $request ) {

		// Get params
		$params = $request->get_params();
		$args = $this->prepare_args( $params );

		// Get latest listing
		$result = GraphQL::getLatestListing( $args );

		// Check error
		if ( is_wp_error( $result ) ) {
			return $result;
		}

		$result = new \WP_REST_Response( $result, 200 );
		$result->set_headers( array( 'Cache-Control' => 'max-age=3600' ) );
		return $result;
	}
}

new Listings();
