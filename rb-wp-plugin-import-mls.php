<?php
/**
 * Plugin Name: RealtyBloc Import MLS
 * Description: RealtyBloc import Data With MLs number
 * Version:     1.0.3
 * Author:      RealtyBloc
 * Author URI:  https://realtybloc.com
 * GitLab Plugin URI: https://gitlab.com/realtybloc/rb-wp-plugin-import-mls
 * Text Domain: rb-wp-plugin-import-mls
 * Domain Path: /rb-wp-plugin-import-mls
 */

// Defines
define( 'RB_PLUGIN_IMPORT_MLS_MAIN_FILE', __FILE__ );
define( 'RB_PLUGIN_IMPORT_MLS_PATH', plugin_dir_path( RB_PLUGIN_IMPORT_MLS_MAIN_FILE ) );
define( 'RB_PLUGIN_IMPORT_MLS_URL', plugins_url( '/', RB_PLUGIN_IMPORT_MLS_MAIN_FILE ) );
define( 'RB_PLUGIN_IMPORT_MLS_VERSION', '1.0.3' );

// Load Plugin Files
include "includes/class-main.php";

