<?php

namespace RB_PLUGIN_IMPORT_MLS\Classes;

use RB_PLUGIN_IMPORT_MLS\Admin\Option;

class GraphQL {
	static $api_url = 'https://realtybloc.xyz/api/';
	static $email;
	static $password;

	/**
	 * GraphQL constructor.
	 */
	public function __construct() {
		// Get option
		$option = Option::get();

		self::$email    = ( isset( $option['api_username'] ) ? $option['api_username'] : '' );
		self::$password = ( isset( $option['api_password'] ) ? $option['api_password'] : '' );
	}

	/**
	 * @return array|mixed|object|\WP_Error
	 */
	public static function get_token() {
		// Check API credential.
		if ( ! self::$email or ! self::$password ) {
			delete_transient( 'rb_api_token_import_mls' );
			return new \WP_Error( 'api-error', __( 'Please enter the API credential.', 'rb-wp-plugin-import-mls' ) );
		}

		if ( false === ( $rb_api_token_import_mls = get_transient( 'rb_api_token_import_mls' ) ) ) {
			$query = sprintf( '{"query": "mutation {\n login(email: \"%s\", password: \"%s\") {\n  success\n  token\n  user\n  errors\n }\n}\n"}', self::$email, self::$password );
			$args  = array(
				'headers' => array( 'Content-Type' => 'application/json' ),
				'body'    => $query
			);

			$result = self::request( $args );
			if ( is_wp_error( $result ) ) {
				return $result;
			}

			// Check credential
			if ( ! $result['data']['login']['success'] ) {
				return new \WP_Error( 'api-error', $result['data']['login']['errors'] );
			}

			$rb_api_token_import_mls = $result['data']['login']['token'];
			$check_token             = self::decode_token( $rb_api_token_import_mls );

			if ( is_wp_error( $check_token ) ) {
				return $check_token;
			}
			// Check token expiration date
			if ( time() > $check_token['exp'] ) {
				delete_transient( 'rb_api_token_import_mls' );
				return self::get_token();
			}
			set_transient( 'rb_api_token_import_mls', $rb_api_token_import_mls, 12 * HOUR_IN_SECONDS );
		}

		return $rb_api_token_import_mls;
	}

	/**
	 * @param $args
	 *
	 * @return array|mixed|object|\WP_Error
	 */
	private static function request( $args ) {
		// Set timeout
		$args['timeout'] = '40';
		$response        = wp_remote_post( self::$api_url, $args );
		if ( is_wp_error( $response ) ) {
			return $response;
		}

		// Check response code
		if ( wp_remote_retrieve_response_code( $response ) != '200' ) {
			$result = json_decode( $response['body'], ARRAY_A );
			if ( isset( $result['errors'][0]['message'] ) ) {
				return new \WP_Error( 'api-error', $result['errors'][0]['message'] );
			}
			return new \WP_Error( 'api-error', json_decode( $result, ARRAY_A ) );
		}

		return json_decode( $response['body'], ARRAY_A );
	}

	/**
	 * @param $token
	 * @return array|\WP_Error
	 */
	public static function decode_token( $token ) {
		try {
			$jwt = \JOSE_JWT::decode( $token );
			return $jwt->claims;
		} catch ( \Exception $e ) {
			return new \WP_Error( 'api-error', $e->getMessage() );
		}
	}

	/**
	 * @param $parameters
	 * @return array|mixed|object|\WP_Error
	 */
	public static function getLatestListing( $parameters = array() ) {
		$token       = self::get_token();
		$api_version = Option::get( 'api_version' ) == 'new' ? 'new' : 'old';
		$api_query   = $api_version == 'new' ? 'VowProperty' : 'ResidentialProperty';

		if ( is_wp_error( $token ) ) {
			return $token;
		}

		$per_page = 20;
		if ( isset( $parameters['per_page'] ) ) {
			$per_page = $parameters['per_page'];
		}

		$page = 0;
		if ( isset( $parameters['page'] ) and $parameters['page'] ) {
			$page = $parameters['page'];
		}
		$page = $per_page * $page;

		$query_param = "";
		$query_param .= 'first: ' . $per_page . ' ';
		$query_param .= 'skip: ' . $page . ' ';
		$query_param .= isset( $parameters['listing_id'] ) ? 'systemId: ' . Helper::explodeArrayIntObject( $parameters['listing_id'] ) . ', ' : '';
		$query_param .= isset( $parameters['search'] ) ? 'search: \"' . urlencode( $parameters['search'] ) . '\", ' : '';
		$query_param .= isset( $parameters['hot_listing_timeline'] ) ? 'listDateMin: \"' . $parameters['hot_listing_timeline'] . '\", ' : '';
		$query_param .= isset( $parameters['min_price'] ) ? 'priceMin: ' . $parameters['min_price'] . ', ' : '';
		$query_param .= isset( $parameters['max_price'] ) ? 'priceMax: ' . $parameters['max_price'] . ', ' : '';
		$query_param .= isset( $parameters['bed_no'] ) ? 'bedNumber: ' . $parameters['bed_no'] . ', ' : '';
		$query_param .= isset( $parameters['bath_no'] ) ? 'bathNumber: ' . $parameters['bath_no'] . ', ' : '';
		$query_param .= isset( $parameters['min_size'] ) ? 'sizeMin: ' . $parameters['min_size'] . ', ' : '';
		$query_param .= isset( $parameters['max_size'] ) ? 'sizeMax: ' . $parameters['max_size'] . ', ' : '';
		$query_param .= isset( $parameters['city'] ) ? 'city: ' . Helper::explodeArrayStringObject( $parameters['city'] ) . ', ' : '';
		$query_param .= isset( $parameters['area'] ) ? 'area: ' . Helper::explodeArrayStringObject( $parameters['area'] ) . ', ' : '';
		$query_param .= isset( $parameters['sub_area'] ) ? 'subArea: ' . Helper::explodeArrayStringObject( $parameters['sub_area'] ) . ', ' : '';
		$query_param .= isset( $parameters['open_house'] ) ? 'openhouse: ' . $parameters['open_house'] . ', ' : '';
		$query_param .= isset( $parameters['agent_id'] ) ? 'agentId: ' . Helper::explodeArrayStringObject( $parameters['agent_id'] ) . ', ' : '';
		$query_param .= isset( $parameters['office_id'] ) ? 'firmId: ' . Helper::explodeArrayIntObject( $parameters['office_id'] ) . ', ' : '';
		$query_param .= isset( $parameters['listing_mls_number'] ) ? 'mlNumber: ' . Helper::explodeArrayStringObject( $parameters['listing_mls_number'] ) . ', ' : '';
		$query_param .= isset( $parameters['listing_type'] ) ? 'propType: ' . Helper::explodeArrayStringObject( $parameters['listing_type'] ) . ', ' : '';
		$query_param .= isset( $parameters['listing_category'] ) ? 'dwelingType: ' . Helper::explodeArrayStringObject( $parameters['listing_category'] ) . ', ' : '';
		$query_param .= isset( $parameters['listing_style'] ) ? 'style: ' . Helper::explodeArrayStringObject( $parameters['listing_style'] ) . ', ' : '';
		$query_param .= isset( $parameters['order_type'] ) ? 'orderBy: ' . $parameters['order_type'] . ', ' : '';
		$query_param .= isset( $parameters['order_sort'] ) ? 'sort: ' . $parameters['order_sort'] . ', ' : '';
		$query_param .= isset( $parameters['listing_postal_code'] ) ? 'postalCode: ' . Helper::explodeArrayStringObject( $parameters['listing_postal_code'] ) . ', ' : '';
		$query_param .= isset( $parameters['latitude'] ) ? 'latitude: ' . $parameters['latitude'] . ', ' : '';
		$query_param .= isset( $parameters['longitude'] ) ? 'longitude: ' . $parameters['longitude'] . ', ' : '';
		$query_param .= isset( $parameters['inside_polygon'] ) ? 'insidePolygon: [' . $parameters['inside_polygon'] . '], ' : '';
		$query_param .= isset( $parameters['distance'] ) ? 'distance: ' . $parameters['distance'] . ', ' : '';
		$query_param .= isset( $parameters['street_dir_prefix'] ) ? 'addressDirection: \"' . $parameters['street_dir_prefix'] . '\", ' : '';
		$query_param .= isset( $parameters['street_desg'] ) ? 'streetDesignation: \"' . $parameters['street_desg'] . '\", ' : '';
		$query_param .= isset( $parameters['street_query'] ) ? 'street: \"' . $parameters['street_query'] . '\", ' : '';
		$query_param .= isset( $parameters['building_number'] ) ? 'addressNumber: \"' . $parameters['building_number'] . '\", ' : '';

		$price_history_fields = $room_fields = '';
		if ( $api_version == 'new' ) {
			$price_history_fields = 'soldPrice\n soldDate\n originalPrice\n expiryDate\n prevPrice\n styleOfHome\n numberOfKitchens\n parking\n typeOfDwelling\n';

			for ( $i = 1; $i <= 28; $i ++ ) {
				$room_fields .= 'room_' . $i . ': room(no:' . $i . ') { dimension1 dimension2 level type } ';
			}

			$query_param .= ( Option::get( 'sold_data_reg_users' ) && isset( $parameters['is_user_logged_in'] ) && $parameters['is_user_logged_in'] == 1 ) ? 'status: [\"1_0\", \"2_0\"], ' : 'status: [\"1_0\"], ';
		}

		$query_param = rtrim( $query_param, ', ' );
		$query       = sprintf( '{"query": "{ ' . $api_query . ' (%s) { pageInfo {total} objects { id\n systemId\n listDate\n modificationDate\n mlNumber\n status\n openhouse {\n id\n }\n listPrice\n location {\n subAreaOrCommunity\n subAreaOrCommunity_compact: subAreaOrCommunity(compact: true)\n area\n area_compact: area(compact: true)\n unit\n number\n streetDesignation\n street\n address\n postalCode\n shortRegionCode\n province\n city\n city_compact: city(compact: true)\n number\n area_compact: area(compact: true)\n area_decoded: area\n coordinate {\n latitude\n longitude\n insideCanada\n }\n }\n firm_1: firm(no: 1) {\n code\n abbreviation\n name\n phoneNumber\n url\n}\n firm_2: firm(no: 2) {\n code\n abbreviation\n name\n phoneNumber\n url\n}\n firm_3: firm(no: 3) {\n code\n abbreviation\n name\n phoneNumber\n url\n}\n info {\n totalBaths\n ' . $price_history_fields . ' ' . $room_fields . ' approxYearBuilt\n virtualTourUrl\n forTaxYear\n styleOfHome\n style: styleOfHome(compact: true)\n totalBedrooms\n typeOfDwelling\n typeOfDwelling_compact: typeOfDwelling(compact: true)\n titleToLand\n view\n noFloorLevels\n fireplaces\n totalBedrooms\n halfBaths\n fullBaths\n approxYearBuilt\n age\n totalBaths\n storeysInBuilding\n propType\n propType_compact: propType(compact: true)\n frontage\n depth\n siteInfluences\n featuresIncluded\n amenities\n viewSpecify\n publicRemarks\n internetRemarks\n virtualTourUrl\n forTaxYear\n grossTaxes\n strataMaintFee\n pubListingOnInternet\n displayAddrOnInternet\n photo{\n urls\n count\n timestamp\n}\n floorArea{\n grandTotal\n finMain\n}\n lot{\n size \n area\n size_square_feet: size(unit: SF),\n size_square_meter: size(unit: SM),\n size_hectars: size(unit: HA)\n}\n }\n agent_1: agent {\n code\n logonName\n phoneNumber\n url\n fullName\n }\n agent_2: agent(no: 2) {\n code\n logonName\n phoneNumber\n url\n fullName\n }\n agent_3: agent(no: 3) {\n code\n logonName\n phoneNumber\n url\n fullName\n}          } } }"}', $query_param );

		$args = array(
			'headers' => array(
				'Content-Type'            => 'application/json',
				'Authorization'           => 'Bearer ' . $token,
				'X-Originating-IP'        => $_SERVER['HTTP_USER_AGENT'],
				'X-Originating-UserAgent' => Helper::getRealIpAddr(),
			),
			'body'    => $query
		);

		$result = self::request( $args );

		if ( is_wp_error( $result ) ) {
			return $result;
		}

		return apply_filters( 'getLatestListing', $result['data'][ $api_query ] );
	}
}

new GraphQL();
