<?php

namespace RB_PLUGIN_IMPORT_MLS\Classes;

use RB_PLUGIN_IMPORT_MLS\Admin\Option;

class Helper {
	/**
	 * @param $array
	 *
	 * @return string
	 */
	public static function implodeArray( $array ) {
		if ( is_array( $array ) ) {
			foreach ( $array as $key => $value ) {
				if ( $value == '' ) {
					unset( $array[ $key ] );
				}
			}

			return implode( ",", $array );
		}

		return $array;
	}

	/**
	 * @param $string
	 *
	 * @return string
	 */
	public static function explodeArrayStringObject( $string ) {
		$data = explode( ',', $string );

		if ( count( $data ) == 1 ) {
			return '\"' . $string . '\"';
		}

		$result = '';

		foreach ( $data as $value ) {
			$result .= '\"' . $value . '\"' . ', ';
		}

		$result = rtrim( $result, ", " );
		$result = '[' . $result . ']';

		return $result;
	}

	/**
	 * @param $string
	 *
	 * @return string
	 */
	public static function explodeArrayIntObject( $string ) {
		$data = explode( ',', $string );

		if ( count( $data ) == 1 ) {
			return $string;
		}

		$result = '';

		foreach ( $data as $value ) {
			$result .= $value . ',';
		}

		$result = rtrim( $result, ", " );
		$result = '[' . $result . ']';

		return $result;
	}

	public static function getPluginVersion() {
		if ( ! function_exists( 'get_plugin_data' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}

		$plugin_data = get_plugin_data( RB_PLUGIN_API_MAIN_FILE );

		if ( isset( $plugin_data['Version'] ) ) {
			return $plugin_data['Version'];
		}
	}

	/**
	 * @param $day
	 *
	 * @return false|string
	 */
	public static function getPreviousDay( $day ) {
		if ( $day ) {
			return date( 'Y-m-d', strtotime( "-{$day} day" ) );
		}
	}

	public static function getRealIpAddr() {
		if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) )   //check ip from share internet
		{
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) )   //to check ip is pass from proxy
		{
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		return $ip;
	}

	/**
	 * @param $term_id
	 * @param $key
	 * @param bool $single
	 *
	 * @return mixed
	 */
	public static function getTermMeta( $term_id, $key, $single = true ) {
		$value = get_term_meta( $term_id, $key, $single );

		return $value;

		/*$replacements = array(
		'VVEGR' => 'VVEGW',
		'VVEHE' => 'VVESU',
		'VLDAI' => 'VLDED',
		'VLDTB' => 'VLDED',
		'VISPA' => 'VWV',
		'VNVHH' => 'VNVHM',
	);

	return str_replace(array_keys($replacements), $replacements, $value);*/
	}

	public static function generateListingURL() {
		return 'search-listings/listings/259957852/Out-of-Town/NO-CITY/32/VOTOT/V982740/';
	}

	/**
	 * @return mixed
	 */
	public static function getPostTypeFromQuery() {
		global $wp_query;

		if ( isset( $wp_query->query['post_type'] ) ) {
			return $wp_query->query['post_type'];
		}
	}

	/**
	 * @param bool $use_default
	 * @return array
	 */
	public static function getCitiesForFilter( $use_default = false ) {
		$cities = get_terms( 'neighbourhood-query', array(
			'orderby'    => 'name',
			'hide_empty' => false,
		) );

		if ( is_wp_error( $cities ) ) {
			return array();
		}

		$cities_array = array();
		$prefix       = 'rb_';

		// Get default cities.
		$option = Option::get( 'listing_default_cities' );

		if ( $use_default and $option ) {
			foreach ( $cities as $city ) {
				$get_term_meta = get_term_meta( $city->term_id, $prefix . 'neighbourhood_query_tax_meta_query_string', true );

				if ( ( array_search( $get_term_meta, $option ) !== false ) and ( $get_term_meta and $city->parent == 0 ) ) {
					$cities_array[] = array(
						'name'   => $city->name,
						'id'     => $city->term_id,
						'query'  => $get_term_meta,
						'parent' => $city->parent,
						'count'  => $city->count,
						'slug'   => $city->slug,
					);
				}
			}
		} else {
			foreach ( $cities as $city ) {
				$get_term_meta = get_term_meta( $city->term_id, $prefix . 'neighbourhood_query_tax_meta_query_string', true );

				if ( $get_term_meta and $city->parent == 0 ) {
					$cities_array[] = array(
						'name'   => $city->name,
						'id'     => $city->term_id,
						'query'  => $get_term_meta,
						'parent' => $city->parent,
						'count'  => $city->count,
						'slug'   => $city->slug,
					);
				}
			}
		}

		return $cities_array;
	}

	/**
	 * @param bool $use_default
	 * @return array
	 */
	public static function getAreasForFilter( $use_default = false ) {
		$areas = get_terms( 'neighbourhood-query', array(
			'orderby'    => 'name',
			'hide_empty' => false,
		) );

		if ( is_wp_error( $areas ) ) {
			return array();
		}

		$areas_array = array();
		$prefix      = 'rb_';

		// Get default cities.
		$option = Option::get( 'listing_default_areas' );

		if ( $use_default and $option ) {
			foreach ( $areas as $area ) {
				$get_term_meta = get_term_meta( $area->term_id, $prefix . 'neighbourhood_query_tax_meta_query_string', true );

				if ( ( array_search( $get_term_meta, $option ) !== false ) and ( $area->parent != 0 ) ) {
					$neighbourhood_parent_info = get_term_by( 'id', $area->parent, 'neighbourhood-query' );

					if ( empty( $neighbourhood_parent_info->parent ) ) {
						$areas_array[] = array(
							'name'   => $area->name,
							'id'     => $area->term_id,
							'query'  => $get_term_meta,
							'parent' => $area->parent,
						);
					}
				}
			}
		} else {
			foreach ( $areas as $area ) {
				$get_term_meta = get_term_meta( $area->term_id, $prefix . 'neighbourhood_query_tax_meta_query_string', true );

				if ( ( $area->parent ) != 0 ) {
					$neighbourhood_parent_info = get_term_by( 'id', $area->parent, 'neighbourhood-query' );

					if ( empty( $neighbourhood_parent_info->parent ) ) {
						$areas_array[] = array(
							'name'   => $area->name,
							'id'     => $area->term_id,
							'query'  => $get_term_meta,
							'parent' => $area->parent,
						);
					}
				}
			}
		}

		return $areas_array;
	}

	/**
	 * @param bool $use_default
	 * @return array
	 */
	public static function getSubAreasForFilter( $use_default = false ) {
		$sub_areas = get_terms( 'neighbourhood-query', array(
			'orderby'    => 'name',
			'hide_empty' => false,
		) );

		if ( is_wp_error( $sub_areas ) ) {
			return array();
		}

		$sub_areas_array = array();
		$prefix          = 'rb_';

		// Get default cities.
		$option = Option::get( 'listing_default_sub_areas' );

		if ( $use_default and $option ) {
			foreach ( $sub_areas as $sub_area ) {
				$get_term_meta = get_term_meta( $sub_area->term_id, $prefix . 'neighbourhood_query_tax_meta_query_string', true );

				if ( ( array_search( $get_term_meta, $option ) !== false ) and ( $sub_area->parent != 0 ) ) {
					$neighbourhood_parent_info = get_term_by( 'id', $sub_area->parent, 'neighbourhood-query' );

					if ( ! empty( $neighbourhood_parent_info->parent ) ) {
						$sub_areas_array[] = array(
							'name'   => $sub_area->name,
							'id'     => $sub_area->term_id,
							'query'  => $get_term_meta,
							'parent' => $sub_area->parent,
							'count'  => $sub_area->count,
							'slug'   => $sub_area->slug,
						);
					}
				}
			}
		} else {
			foreach ( $sub_areas as $sub_area ) {
				$get_term_meta = get_term_meta( $sub_area->term_id, $prefix . 'neighbourhood_query_tax_meta_query_string', true );

				if ( ( $sub_area->parent ) != 0 ) {
					$neighbourhood_parent_info = get_term_by( 'id', $sub_area->parent, 'neighbourhood-query' );

					if ( ! empty( $neighbourhood_parent_info->parent ) ) {
						$sub_areas_array[] = array(
							'name'   => $sub_area->name,
							'id'     => $sub_area->term_id,
							'query'  => $get_term_meta,
							'parent' => $sub_area->parent,
							'count'  => $sub_area->count,
							'slug'   => $sub_area->slug,
						);
					}
				}
			}
		}


		return $sub_areas_array;
	}

	public static function getListingPropertyTypiesForFilter() {
		$listing_property_typies = get_terms( 'property-type', array(
			'orderby'    => 'count',
			'hide_empty' => false,
		) );

		if ( is_wp_error( $listing_property_typies ) ) {
			return array();
		}

		$listing_property_typies_array = array();
		$prefix                        = 'rb_';

		foreach ( $listing_property_typies as $listing_property_type ) {
			$get_term_meta                              = get_term_meta( $listing_property_type->term_id, $prefix . 'property_type_tax_meta_query_string', true );
			$tax_meta                                   = ! empty( $get_term_meta ) ? $get_term_meta : null;
			$listing_property_typies_array[ $tax_meta ] = $listing_property_type->name;
		}

		return $listing_property_typies_array;
	}

	public static function getListingDwellingTypiesForFilter() {
		$listing_dwelling_typies = get_terms( 'dwelling-type', array(
			'orderby'    => 'count',
			'hide_empty' => false,
		) );

		if ( is_wp_error( $listing_dwelling_typies ) ) {
			return array();
		}

		$prefix                        = 'rb_';
		$listing_dwelling_typies_array = array();

		foreach ( $listing_dwelling_typies as $listing_dwelling_type ) {
			$get_term_meta                              = get_term_meta( $listing_dwelling_type->term_id, $prefix . 'dwelling_type_tax_meta_query_string', true );
			$tax_meta                                   = ! empty( $get_term_meta ) ? $get_term_meta : null;
			$listing_dwelling_typies_array[ $tax_meta ] = $listing_dwelling_type->name;
		}

		return $listing_dwelling_typies_array;
	}

	public static function getListingPropertyStyliesForFilter() {
		$listing_property_stylies = get_terms( 'property-style', array(
			'orderby'    => 'count',
			'hide_empty' => false,
		) );

		if ( is_wp_error( $listing_property_stylies ) ) {
			return array();
		}

		$listing_property_stylies_array = array();
		$prefix                         = 'rb_';

		foreach ( $listing_property_stylies as $listing_property_style ) {
			$get_term_meta                               = get_term_meta( $listing_property_style->term_id, $prefix . 'property_style_tax_meta_query_string', true );
			$tax_meta                                    = ! empty( $get_term_meta ) ? $get_term_meta : null;
			$listing_property_stylies_array[ $tax_meta ] = $listing_property_style->name;
		}

		return $listing_property_stylies_array;
	}

	/**
	 * @return array
	 */
	public static function getCitiesForFilterArray() {
		$get_cities_for_filter       = Helper::getCitiesForFilter();
		$get_cities_for_filter_array = array();
		foreach ( $get_cities_for_filter as $cities_array ) {
			$get_cities_for_filter_array[ $cities_array['query'] ] = $cities_array['name'];
		}

		return $get_cities_for_filter_array;
	}

	/**
	 * @return array
	 */
	public static function getAreasForFilterArray() {
		$get_areas_for_filter       = Helper::getAreasForFilter();
		$get_areas_for_filter_array = array();
		foreach ( $get_areas_for_filter as $areas_array ) {
			$get_areas_for_filter_array[ $areas_array['query'] ] = $areas_array['name'];
		}

		return $get_areas_for_filter_array;
	}

	public static function getSubAreasForFilterArray() {
		$get_sub_areas_for_filter       = Helper::getSubAreasForFilter();
		$get_sub_areas_for_filter_array = array();
		foreach ( $get_sub_areas_for_filter as $sub_areas_array ) {
			$get_sub_areas_for_filter_array[ $sub_areas_array['query'] ] = $sub_areas_array['name'];
		}

		return $get_sub_areas_for_filter_array;
	}

	public static function getPages() {
		$args = array(
			'posts_per_page' => - 1,
			'post_type'      => 'page',
			'post_status'    => 'publish'
		);

		$pages = array();
		foreach ( get_pages( $args ) as $page ) {
			$pages[ $page->ID ] = $page->post_title;
		}

		return $pages;
	}

	public static function getTimeAgo( $time, $tense = 'ago' ) {
		// declaring periods as static function var for future use
		static $periods = array( 'year', 'month', 'day', 'hour', 'minute', 'second' );

		// checking time format
		if ( ! ( strtotime( $time ) > 0 ) ) {
			return false;
		}

		// getting diff between now and time
		$now  = new \DateTime( 'now' );
		$time = new \DateTime( $time );
		$diff = $now->diff( $time )->format( '%y %m %d %h %i %s' );
		// combining diff with periods
		$diff = explode( ' ', $diff );
		$diff = array_combine( $periods, $diff );
		// filtering zero periods from diff
		$diff = array_filter( $diff );
		// getting first period and value
		$period = key( $diff );
		$value  = current( $diff );

		// if input time was equal now, value will be 0, so checking it
		if ( ! $value ) {
			$period = 'seconds';
			$value  = 0;
		} else {
			// converting days to weeks
			if ( $period == 'day' && $value >= 7 ) {
				$period = 'week';
				$value  = floor( $value / 7 );
			}
			// adding 's' to period for human readability
			if ( $value > 1 ) {
				$period .= 's';
			}
		}

		// returning timeago
		return "$value $period $tense";
	}
}
