<?php

namespace RB_PLUGIN_IMPORT_MLS\Admin;

class Setting {
	/**
	 * @var string
	 */
	static $key = 'mls_import_option';

	public function __construct() {
		add_action( 'cmb2_admin_init', array( $this, 'addGeneralOptionFields' ) );
	}

	public static function field_list() {
		return array(
			'address'           => array( 'name' => __( 'Property Address', 'rb-wp-plugin-import-mls' ), 'type' => 'textarea', 'default' => 'property_address' ),
//			'title'             => array( 'name' => __( 'Property Additional Title', 'rb-wp-plugin-import-mls' ), 'type' => 'textarea', 'default' => 'property_additional_title' ),
			'postal_code'       => array( 'name' => __( 'Zip/Postal code', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_postal_code' ),
			'post_title'        => array( 'name' => __( 'Post title', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'post_title' ),
			'post_content'      => array( 'name' => __( 'Post Content', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'content' ),
			'mls_number'        => array( 'name' => __( 'MLS Number', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_mls_number' ),
			'brokerage_name'        => array( 'name' => __( 'Brokerage Name', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_brokerage_name' ),
			'frontage'        => array( 'name' => __( 'Frontage', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_frontage_size' ),
			'open_virtual_tour' => array( 'name' => __( 'Virtual Tour', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_open_virtual_tour' ),
			'price'             => array( 'name' => __( 'Price', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_price' ),
			'bedrooms_number'   => array( 'name' => __( 'Bedrooms Number', 'rb-wp-plugin-import-mls' ), 'type' => 'select', 'default' => 'property_bedrooms_number' ),
			'bathrooms_number'  => array( 'name' => __( 'Bathrooms Number', 'rb-wp-plugin-import-mls' ), 'type' => 'select', 'default' => 'property_bathrooms_number' ),
			'lot_size'          => array( 'name' => __( 'Lot Size', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'lot_size' ),
			'style'             => array( 'name' => __( 'Style', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_style' ),
			'depth_size'        => array( 'name' => __( 'Depth', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_depth_size' ),
			'view'              => array( 'name' => __( 'View', 'rb-wp-plugin-import-mls' ), 'type' => 'radio', 'default' => 'property_view' ),
			'view_specify'      => array( 'name' => __( 'View Specify', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_view_specify' ),
			'built_year'        => array( 'name' => __( 'Year of Built', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_built_year' ),
			'maintenance_fee'   => array( 'name' => __( 'Maintenance Fee', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_maintenance_fee' ),
			'tax_year'          => array( 'name' => __( 'Tax Year', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_tax_year' ),
			'tax'          			=> array( 'name' => __( 'Tax', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_tax' ),
			'amenities'         => array( 'name' => __( 'Amenities', 'rb-wp-plugin-import-mls' ), 'type' => 'tinymce', 'default' => 'property_amenities' ),
			'features'          => array( 'name' => __( 'Features', 'rb-wp-plugin-import-mls' ), 'type' => 'tinymce', 'default' => 'property_features' ),
			'latitude'          => array( 'name' => __( 'Latitude', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_latitude' ),
			'property_size'     => array( 'name' => __( 'Propery Size', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_size' ),
			'longitude'         => array( 'name' => __( 'Longitude', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_longitude' ),
			'gallery'           => array( 'name' => __( 'Gallery', 'rb-wp-plugin-import-mls' ), 'type' => 'text', 'default' => 'property_gallery' ),
		);
	}

	/**
	 * Add the options metabox to the array of metaboxes
	 * @since  0.1.0
	 */
	public function addGeneralOptionFields() {

		// hook in our save notices
		add_action( "cmb2_save_options-mls_import_option_options", array( 'Setting', 'settings_notices' ), 10, 2 );

		$cmb = new_cmb2_box( array(
			'id'           => 'mls_import_option',
			'title'        => esc_html__( 'Import MLS', 'myprefix' ),
			'object_types' => array( 'options-page' ),
			'option_key'   => self::$key, // The option key and admin menu page slug.
			'icon_url'     => 'dashicons-hammer',
			// 'position'        => 1, // Menu position. Only applicable if 'parent_slug' is left empty.
			// 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
			'save_button'  => esc_html__( 'Save Options', 'myprefix' ), // The text for the options-page save button. Defaults to 'Save'.
		) );

		$cmb->add_field( array(
			'name' => 'API Credential',
			//'desc' => '',
			'type' => 'title',
			'id'   => 'api_title'
		) );

		$cmb->add_field( array(
			'name'    => __( 'Username', 'rb-wp-plugin-import-mls' ),
			'desc'    => __( 'The username required for get data from IDX', 'rb-wp-plugin-import-mls' ),
			'id'      => 'api_username',
			'default' => '',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name'    => __( 'Password', 'rb-wp-plugin-import-mls' ),
			'desc'    => __( 'The password required for get data from IDX', 'rb-wp-plugin-import-mls' ),
			'id'      => 'api_password',
			'default' => '',
			'type'    => 'text',
		) );

		$cmb->add_field( array(
			'name'    => __( 'API Version', 'rb-wp-plugin-import-mls' ),
			'id'      => 'api_version',
			'type'    => 'select',
			'options' => array(
				'old' => __( 'Old', 'rb-wp-plugin-import-mls' ),
				'new' => __( 'New', 'rb-wp-plugin-import-mls' ),
			)
		) );

		$cmb->add_field( array(
			'name' => 'Field name',
			'type' => 'title',
			'id'   => 'field_name'
		) );

		$default_prefix = '_rb_';
		foreach ( self::field_list() as $f => $v ) {
			$cmb->add_field( array(
				'name'    => $v['name'],
				'desc'    => ( $f == "gallery" ? "Gallery type is array of image" : "" ),
				'id'      => 'field_' . $f,
				'default' => ($v['default'] == "post_title" || $v['default'] == "content") ? $v['default'] : $default_prefix . $v['default'],
				'type'    => 'text',
			) );
			if ( $f != "gallery" ) {
				$cmb->add_field( array(
					'name'    => 'Field Type',
					'desc'    => $v['name'] . ' field',
					'id'      => 'field_type_' . $f,
					'type'    => 'select',
					'default' => $v['type'],
					'options' => array(
						'text'     => __( 'text', 'rb-wp-plugin-import-mls' ),
						'textarea' => __( 'textarea', 'rb-wp-plugin-import-mls' ),
						'tinymce'  => __( 'TinyMCE', 'rb-wp-plugin-import-mls' ),
						'checkbox' => __( 'checkbox', 'rb-wp-plugin-import-mls' ),
						'radio' => __( 'radio', 'rb-wp-plugin-import-mls' ),
						'select'   => __( 'select', 'rb-wp-plugin-import-mls' )
					),
				) );
			}
		}


	}

	/**
	 * Register settings notices for display
	 *
	 * @since  0.1.0
	 *
	 * @param  int $object_id Option key
	 * @param  array $updated Array of updated fields
	 *
	 * @return void
	 */
	public static function settings_notices( $object_id, $updated ) {
		if ( $object_id !== self::$key || empty( $updated ) ) {
			return;
		}

		add_settings_error( self::$key . '-notices', '', __( 'Settings updated.', 'rb-wp-plugin-import-mls' ), 'updated' );
		settings_errors( self::$key . '-notices' );
	}
}

new Setting();
