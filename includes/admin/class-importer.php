<?php

class Importer {
	/**
	 * @var string
	 */
	public $prefix;

	/**
	 * Property constructor.
	 */
	public function __construct() {
		$this->prefix = '_rb_';

		// Enqueue scripts.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		// CMB2 fields.
		add_action( 'cmb2_admin_init', array( $this, 'init_metabox' ) );

		// Ajax action.
		add_action( 'wp_ajax_rb_upload_image', array( $this, 'ajax_upload_image_callback' ) );
		add_action( 'wp_ajax_nopriv_rb_upload_image', array( $this, 'ajax_upload_image_callback' ) );
	}

	/**
	 * Enqueue our script when needed.
	 */
	public function enqueue_scripts() {
		$screen = get_current_screen();
		if ( ! isset( $screen->post_type ) || 'property' !== $screen->post_type ) {
			return;
		}

		// Get Fields Type
		$fields = array();
		$opt    = \RB_PLUGIN_IMPORT_MLS\Admin\Option::get();
		foreach ( \RB_PLUGIN_IMPORT_MLS\Admin\Setting::field_list() as $f => $v ) {
			$fields[ $f ] = array( 'name' => $opt[ 'field_' . $f ], 'type' => ( isset( $opt[ 'field_type_' . $f ] ) ? $opt[ 'field_type_' . $f ] : 'text' ) );
		}

		wp_enqueue_script( 'rb-importer-script', RB_PLUGIN_IMPORT_MLS_URL . 'assets/js/importer.js', array( 'jquery' ), RB_PLUGIN_IMPORT_MLS_VERSION );
		wp_localize_script( 'rb-importer-script', 'rb_rest_api', array( 'rest_url' => get_rest_url( null, 'mls/v1/listing' ), 'fields' => $fields ) );
	}

	public function init_metabox() {
		$cmb = new_cmb2_box( array(
			'id'           => $this->prefix . 'property_api_metaboxe',
			'title'        => __( 'Import Data', 'base-plugin' ),
			'object_types' => array( 'property' ),
			'context'      => 'side',
			'priority'     => 'high',
		) );

		$cmb->add_field( array(
			'name'        => __( 'MLS Number', 'base-plugin' ),
			'desc'        => __( 'Please enter the MLS number for import.', 'base-plugin' ),
			'id'          => $this->prefix . 'mls_number',
			'type'        => 'text',
			'after_field' => '<img id="rb-api-loading" alt="loading" style="margin-top:11px;display:none;" src="' . RB_PLUGIN_IMPORT_MLS_URL . 'assets/images/loading.gif"/><button id="rb-api-import" class="preview button">import</button><div class="wp-clearfix"></div><div style="background: #4eb719;padding: 10px;border-radius: 5px;margin: 15px 0px 0px 0px;color: #fff; display: none;" id="success_mls_import"></div>',
		) );
	}

	public function ajax_upload_image_callback() {
		if ( empty( $_REQUEST['urls'] ) ) {
			wp_send_json( array( 'error' => true, 'message' => 'The urls not found!' ) );
			exit;
		}

		$urls           = $_REQUEST['urls'];
		$post_id        = ( isset( $_REQUEST['post_id'] ) ? $_REQUEST['post_id'] : 0 );
		$attachment_ids = array();

		foreach ( $urls as $url ) {
			$attachment_id = $this->upload_image( $post_id, $url );

			if ( is_wp_error( $attachment_id ) ) {
				wp_send_json( array( 'error' => true, 'message' => $attachment_id->get_error_message() ) );
				exit;
			}

			$attachment_ids[ $attachment_id ] = wp_get_attachment_url( $attachment_id );
		}

		wp_send_json( array( 'error' => false, 'data' => $attachment_ids ) );
		exit;
	}

	/**
	 * @param $postID
	 * @param $url
	 * @param string $alt
	 * @return int|object
	 */
	private function upload_image( $postID, $url, $alt = '' ) {
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );

		$tmp        = download_url( $url );
		$desc       = $alt;
		$file_array = array();

		// Set variables for storage
		// fix file filename for query strings
		preg_match( '/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $url, $matches );
		$file_array['name']     = basename( $matches[0] );
		$file_array['tmp_name'] = $tmp;

		// If error storing temporarily, unlink
		if ( is_wp_error( $tmp ) ) {
			@unlink( $file_array['tmp_name'] );
			$file_array['tmp_name'] = '';
		}

		// do the validation and storage stuff
		$id = media_handle_sideload( $file_array, $postID, $desc );

		// If error storing permanently, unlink
		if ( is_wp_error( $id ) ) {
			@unlink( $file_array['tmp_name'] );
			return $id;
		}

		return $id;
	}
}

new Importer();
