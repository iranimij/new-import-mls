<?php

namespace RB_PLUGIN_IMPORT_MLS\Admin;

use RB_PLUGIN_IMPORT_MLS\Classes\Core;

class Option {
	/**
	 * @param string $option_name
	 *
	 * @param string $default
	 * @return mixed|void
	 */
	public static function get( $option_name = '', $default = '' ) {
		if ( ! Core::$old_option ) {
			Core::$old_option = array();
		}

		if ( ! is_array( Core::$options ) ) {
			if ( $default ) {
				return $default;
			}

			return array();
		}

		// Convert on to 1
		foreach ( Core::$options as $key => $value ) {
			if ( $value == 'on' ) {
				Core::$options[ $key ] = '1';
			}
		}

		// Merge options
		$options = array_merge( Core::$options, Core::$old_option );

		// Check the option is single
		if ( $option_name ) {
			if ( empty( $options[ $option_name ] ) ) {
				return $default;
			}

			return $options[ $option_name ];
		}

		if ( ! $options and $default ) {
			return $default;
		}

		return $options;
	}

	/**
	 * @param $postID
	 * @param $fieldName
	 *
	 * @return mixed
	 */
	public static function getField( $postID, $fieldName = '' ) {
		$value = get_post_meta( $postID, $fieldName, true );

		if ( $value == 'on' ) {
			return 1;
		}

		if ( $value ) {
			return $value;
		}
	}
}
