<?php

namespace RB_PLUGIN_IMPORT_MLS\Classes;

/**
 * Class Listing
 * @package RB_Plugin_API\Classes
 */
class Listing {
	/**
	 * @var array|mixed|object|\WP_Error
	 */
	private $data;

	/**
	 * Listing constructor.
	 * @param $id
	 */
	public function __construct( $id ) {
		$this->data = self::getSingleListing( $id );
	}

	public function hasItem() {
		if ( $this->data ) {
			return true;
		}
	}

	/**
	 * @param bool $formatted
	 * @return mixed|string
	 */
	public function getPrice( $formatted = false ) {
		$price = isset( $this->data['listPrice'] ) ? $this->data['listPrice'] : 0;
		if ( $formatted ) {
			return '$' . number_format( $price );
		}

		return $price;
	}

	/**
	 * @param bool $ago
	 * @return mixed|string
	 * @throws \Exception
	 */
	public function getDate( $ago = false ) {
		if ( empty( $this->data['listDate'] ) or ! $this->data['listDate'] ) {
			return '';
		}

		if ( $ago ) {
			$date = new \DateTime( $this->data['listDate'] );
			return Helper::getTimeAgo( $date->format( 'Y-m-d' ) );
		} else {
			return $this->data['listDate'];
		}

	}

	/**
	 * @return string
	 * @throws \Exception
	 */
	public function getLastUpdate() {
		if ( empty( $this->data['modificationDate'] ) or ! $this->data['modificationDate'] ) {
			return '';
		}

		$date = new \DateTime( $this->data['modificationDate'] );

		return Helper::getTimeAgo( $date->format( 'Y-m-d' ) );
	}

	/**
	 * @return mixed|string
	 */
	public function getMLNumber() {
		return isset( $this->data['mlNumber'] ) ? $this->data['mlNumber'] : '';
	}

	/**
	 * @return mixed|string
	 */
	public function getStatus() {
		return isset( $this->data['status'] ) ? $this->data['status'] : '';
	}

	/**
	 * @param bool $compact
	 * @return string
	 */
	public function getSubArea( $compact = false ) {
		if ( $compact ) {
			return isset( $this->data['location']['subAreaOrCommunity_compact'] ) ? $this->data['location']['subAreaOrCommunity_compact'] : '';
		} else {
			return isset( $this->data['location']['subAreaOrCommunity'] ) ? $this->data['location']['subAreaOrCommunity'] : '';
		}
	}

	/**
	 * @param bool $compact
	 * @return string
	 */
	public function getArea( $compact = false ) {
		if ( $compact ) {
			return isset( $this->data['location']['area_compact'] ) ? $this->data['location']['area_compact'] : '';
		} else {
			return isset( $this->data['location']['area'] ) ? $this->data['location']['area'] : '';
		}
	}

	/**
	 * @return string
	 */
	public function getAddress() {
		return isset( $this->data['location']['address'] ) ? $this->data['location']['address'] : '';
	}

	/**
	 * @return string
	 */
	public function getPostalCode() {
		return isset( $this->data['location']['postalCode'] ) ? $this->data['location']['postalCode'] : '';
	}

	/**
	 * @return string
	 */
	public function getCity() {
		return isset( $this->data['location']['city'] ) ? $this->data['location']['city'] : '';
	}

	/**
	 * @return string
	 */
	public function getCoordinate() {
		return isset( $this->data['location']['coordinate'] ) ? $this->data['location']['coordinate'] : '';
	}

	/**
	 * @return string
	 */
	public function getImages() {
		return isset( $this->data['info']['photo']['urls'] ) ? $this->data['info']['photo']['urls'] : '';
	}

	/**
	 * @return string
	 */
	public function getBedrooms() {
		return isset( $this->data['info']['totalBedrooms'] ) ? $this->data['info']['totalBedrooms'] : '';
	}

	/**
	 * @return string
	 */
	public function getBaths() {
		return isset( $this->data['info']['totalBaths'] ) ? $this->data['info']['totalBaths'] : '';
	}

	/**
	 * @return string
	 */
	public function getSquareFoot() {
		return isset( $this->data['location']['unit'] ) ? $this->data['location']['unit'] : '';
	}

	/**
	 * @return string
	 */
	public function getInfo() {
		return isset( $this->data['info']['publicRemarks'] ) ? $this->data['info']['publicRemarks'] : '';
	}

	/**
	 * @param bool $compact
	 * @return string
	 */
	public function getType( $compact = false ) {
		if ( $compact ) {
			return isset( $this->data['info']['typeOfDwelling_compact'] ) ? $this->data['info']['typeOfDwelling_compact'] : '';
		} else {
			return isset( $this->data['info']['typeOfDwelling'] ) ? $this->data['info']['typeOfDwelling'] : '';
		}
	}

	/**
	 * @return string
	 */
	public function getPropType() {
		return isset( $this->data['info']['propType'] ) ? $this->data['info']['propType'] : '';
	}

	/**
	 * @return string
	 */
	public function getAge() {
		return isset( $this->data['info']['age'] ) ? $this->data['info']['age'] : '';
	}

	/**
	 * @return string
	 */
	public function getFeatures() {
		return isset( $this->data['info']['featuresIncluded'] ) ? $this->data['info']['featuresIncluded'] : '';
	}

	/**
	 * @return string
	 */
	public function getAmenities() {
		return isset( $this->data['info']['amenities'] ) ? $this->data['info']['amenities'] : '';
	}

	/**
	 * @return string
	 */
	public function getYear() {
		return isset( $this->data['info']['approxYearBuilt'] ) ? $this->data['info']['approxYearBuilt'] : '';
	}

	/**
	 * @param bool $formatted
	 * @return string
	 */
	public function getMaintFee( $formatted = false ) {
		$price = isset( $this->data['info']['strataMaintFee'] ) ? $this->data['info']['strataMaintFee'] : 0;

		if ( $formatted ) {
			return '$' . number_format( $price );
		}

		return $price;
	}

	/**
	 * @param bool $formatted
	 * @return string
	 */
	public function getGrossTaxes( $formatted = false ) {
		$price = isset( $this->data['info']['grossTaxes'] ) ? $this->data['info']['grossTaxes'] : '';

		if ( $formatted ) {
			return '$' . number_format( $price );
		}

		return $price;
	}

	/**
	 * @return string
	 */
	public function getViewSpecify() {
		return isset( $this->data['info']['viewSpecify'] ) ? $this->data['info']['viewSpecify'] : '';
	}

	/**
	 * @return string
	 */
	public function getLandTitle() {
		return isset( $this->data['info']['titleToLand'] ) ? $this->data['info']['titleToLand'] : '';
	}

	/**
	 * @return string
	 */
	public function getVirtualTour() {
		return isset( $this->data['info']['virtualTourUrl'] ) ? $this->data['info']['virtualTourUrl'] : '';
	}

	/**
	 * @return string
	 */
	public function getMlsNumber() {
		return isset( $this->data['mlNumber'] ) ? $this->data['mlNumber'] : '';
	}

	/**
	 * @return string
	 */
	public function getSystemId() {
		return isset( $this->data['systemId'] ) ? $this->data['systemId'] : '';
	}

	/**
	 * @return string
	 */
	public function getLotSize() {
		return isset( $this->data['info']['lot']['size'] ) ? $this->data['info']['lot']['size'] : '';
	}

	/**
	 * @return string
	 */
	public function getFloorArea() {
		return isset( $this->data['info']['floorArea']['grandTotal'] ) ? $this->data['info']['floorArea']['grandTotal'] : '';
	}

	/**
	 * @return string
	 */
	public function getOffice() {
		return isset( $this->data['firm_1']['name'] ) ? $this->data['firm_1']['name'] : '';
	}

	/**
	 * @return string
	 */
	public function getStyle() {
		return isset( $this->data['info']['styleOfHome'] ) ? $this->data['info']['styleOfHome'] : '';
	}

	/**
	 * @return string
	 */
	public function getParking() {
		return isset( $this->data['info']['parking'] ) ? $this->data['info']['parking'] : '';
	}

	/**
	 * @return string
	 */
	public function getKitchen() {
		return isset( $this->data['info']['numberOfKitchens'] ) ? $this->data['info']['numberOfKitchens'] : '';
	}

	/**
	 * @return array
	 */
	public function getRooms() {
		$rooms = [];

		for ( $i = 1; $i <= 28; $i ++ ) {
			if ( ! empty( $this->data['info'][ 'room_' . $i ] ) ) {
				if ( ! empty( $this->data['info'][ 'room_' . $i ]['type'] ) || ! empty( $this->data['info'][ 'room_' . $i ]['level'] ) ) {
					$rooms[ $i ] = $this->data['info'][ 'room_' . $i ];
				}
			}
		}

		return $rooms;
	}

	/**
	 * @return array
	 */
	public function getPrices() {
		$prices = [];

		if ( ! empty( $this->data['listPrice'] ) ) {
			$prices['current']['price'] = $this->data['listPrice'];
		}

		if ( ! empty( $this->data['info']['expiryDate'] ) ) {
			$prices['current']['date'] = $this->data['info']['expiryDate'];
		}

		if ( ! empty( $this->data['info']['originalPrice'] ) ) {
			$prices['original']['price'] = $this->data['info']['originalPrice'];
		}

		if ( ! empty( $this->data['listDate'] ) ) {
			$prices['original']['date'] = $this->data['listDate'];
		}

		if ( ! empty( $this->data['info']['prevPrice'] ) ) {
			$prices['prev']['price'] = $this->data['info']['prevPrice'];
		}

		if ( ! empty( $this->data['info']['soldPrice'] ) ) {
			$prices['sold']['price'] = $this->data['info']['soldPrice'];
		}

		if ( ! empty( $this->data['info']['soldDate'] ) ) {
			$prices['sold']['date'] = $this->data['info']['soldDate'];
		}

		return $prices;
	}

	/**
	 * @param $id
	 * @param $search_by_mls
	 * @return array|mixed|object|\WP_Error
	 */
	public static function getSingleListing( $id, $search_by_mls = false ) {
		if ( $search_by_mls ) {
			$args = array(
				'listing_mls_number' => $id,
			);
		} else {
			$args = array(
				'listing_id' => $id,
			);
		}

		if ( is_user_logged_in() ) {
			$args['is_user_logged_in'] = 1;
		}

		// Get latest listing
		$result = GraphQL::getLatestListing( $args );

		// Check error
		if ( is_wp_error( $result ) ) {
			return $result;
		}

		if ( isset( $result['objects'][0] ) ) {
			$result = $result['objects'][0];
		}

		return $result;
	}
}
