<?php

namespace RB_PLUGIN_IMPORT_MLS\Classes\API;

use RB_PLUGIN_IMPORT_MLS\Admin\Option;
use RB_PLUGIN_IMPORT_MLS\Classes\Helper;

class REST {
	/**
	 * @return bool
	 */
	public function has_access() {
		return true;
	}

	/**
	 * @param $params
	 * @return array
	 */
	public function prepare_args( $params ) {
		$args = array();

		if ( isset( $params['id'] ) and $params['id'] ) {
			$args['id'] = esc_sql( $params['id'] );
		}

		if ( isset( $params['search'] ) and $params['search'] ) {
			$args['search'] = esc_sql( $params['search'] );
		}

		if ( isset( $params['per_page'] ) and $params['per_page'] ) {
			$args['per_page'] = esc_sql( $params['per_page'] );
		}

		if ( isset( $params['page'] ) and $params['page'] ) {
			$args['page'] = esc_sql( $params['page'] );
		}

		if ( isset( $params['min_price'] ) and $params['min_price'] ) {
			$args['min_price'] = esc_sql( $params['min_price'] );
		}

		if ( isset( $params['max_price'] ) and $params['max_price'] ) {
			$args['max_price'] = esc_sql( $params['max_price'] );
		}

		if ( isset( $params['bed_no'] ) and $params['bed_no'] ) {
			$args['bed_no'] = esc_sql( $params['bed_no'] );
		}

		if ( isset( $params['bath_no'] ) and $params['bath_no'] ) {
			$args['bath_no'] = esc_sql( $params['bath_no'] );
		}

		if ( isset( $params['min_size'] ) and $params['min_size'] ) {
			$args['min_size'] = esc_sql( $params['min_size'] );
		}

		if ( isset( $params['max_size'] ) and $params['max_size'] ) {
			$args['max_size'] = esc_sql( $params['max_size'] );
		}

		if ( isset( $params['city'] ) and $params['city'] ) {
			$args['city'] = esc_sql( $params['city'] );
		}

		if ( isset( $params['area'] ) and $params['area'] ) {
			$args['area'] = esc_sql( $params['area'] );
		}

		if ( isset( $params['sub_area'] ) and $params['sub_area'] ) {
			$args['sub_area'] = esc_sql( $params['sub_area'] );
		}

		if ( isset( $params['open_house'] ) and $params['open_house'] ) {
			$args['open_house'] = esc_sql( $params['open_house'] );
		}

		if ( isset( $params['is_user_logged_in'] ) and $params['is_user_logged_in'] ) {
			$args['is_user_logged_in'] = esc_sql( $params['is_user_logged_in'] );
		}

		if ( isset( $params['agent_id'] ) and $params['agent_id'] ) {
			$args['agent_id'] = esc_sql( $params['agent_id'] );
		}

		if ( isset( $params['office_id'] ) and $params['office_id'] ) {
			$args['office_id'] = esc_sql( $params['office_id'] );
		}

		if ( isset( $params['listing_id'] ) and $params['listing_id'] ) {
			$args['listing_id'] = esc_sql( $params['listing_id'] );
		}

		if ( isset( $params['listing_mls_number'] ) and $params['listing_mls_number'] ) {
			$args['listing_mls_number'] = esc_sql( $params['listing_mls_number'] );
		}

		if ( isset( $params['listing_type'] ) and $params['listing_type'] ) {
			$args['listing_type'] = esc_sql( $params['listing_type'] );
		}

		if ( isset( $params['listing_category'] ) and $params['listing_category'] ) {
			$args['listing_category'] = esc_sql( $params['listing_category'] );
		}

		if ( isset( $params['listing_style'] ) and $params['listing_style'] ) {
			$args['listing_style'] = esc_sql( $params['listing_style'] );
		}

		if ( isset( $params['order_type'] ) and $params['order_type'] ) {
			$args['order_type'] = esc_sql( $params['order_type'] );
		}

		if ( isset( $params['order_sort'] ) and $params['order_sort'] ) {
			$args['order_sort'] = esc_sql( $params['order_sort'] );
		}

		if ( isset( $params['listing_postal_code'] ) and $params['listing_postal_code'] ) {
			$args['listing_postal_code'] = esc_sql( $params['listing_postal_code'] );
		}

		if ( isset( $params['hot_listing_timeline'] ) and $params['hot_listing_timeline'] ) {
			$args['hot_listing_timeline'] = Helper::getPreviousDay( $params['hot_listing_timeline'] );
		}

		if ( isset( $params['latitude'] ) and $params['latitude'] ) {
			$args['latitude'] = esc_sql( $params['latitude'] );
		}

		if ( isset( $params['longitude'] ) and $params['longitude'] ) {
			$args['longitude'] = esc_sql( $params['longitude'] );
		}

		if ( isset( $params['distance'] ) and $params['distance'] ) {
			$args['distance'] = esc_sql( $params['distance'] );
		}

		if ( isset( $params['name'] ) and $params['name'] ) {
			$args['name'] = $params['name'];
		}

		if ( isset( $params['inside_polygon'] ) and $params['inside_polygon'] ) {
			$args['inside_polygon'] = $params['inside_polygon'];
		}

		if ( isset( $params['district'] ) and $params['district'] ) {
			$args['district'] = $params['district'];
		}

		return $args;
	}

	/**
	 * @param $post_id
	 * @param $key
	 * @return mixed|string
	 */
	public function get_field( $post_id, $key ) {
		$value = Option::getField( $post_id, '_rb_' . $key );

		if ( ! $value ) {
			return '';
		}

		return $value;
	}

	/**
	 * @param $post_id
	 * @return mixed
	 */
	public function get_thumbnail( $post_id ) {
		$attachment = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'medium', true );

		if ( isset( $attachment[0] ) ) {
			return $attachment[0];
		}
	}

	/**
	 * @param $post_id
	 * @param $key
	 * @return mixed
	 */
	public function get_gallery( $post_id, $key ) {
		$value = Option::getField( $post_id, '_rb_' . $key );

		if ( $value and is_array( $value ) ) {
			sort( $value );
			return $value;
		}
	}

	/**
	 * @param $post_id
	 * @return array
	 */
	public function get_building_tags( $post_id ) {
		$result = wp_get_post_terms( $post_id, 'building-tags', array( "fields" => "all" ) );

		if ( is_wp_error( $result ) ) {
			return array();
		}

		$data = array();
		foreach ( $result as $item ) {
			$data[] = $item->name;
		}

		return $data;
	}
}
